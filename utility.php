<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function __autoload_elastica($class) {

    $path = str_replace('\\', '/', $class);
    $plugin_dir = dirname(__FILE__) . '/Elastica/lib';
    if (file_exists($plugin_dir . '/' . $path . '.php')) {
        require_once($plugin_dir . '/' . $path . '.php');
    }
}

function clean_description($content){
    $str_array = array('\t','\n','\r');
    return str_replace($str_array, null, $content);
}

function clean_title($content) {
    $content = html_entity_decode($content);
    $content = strip_tags($content);
    if (!mb_check_encoding($content, 'UTF-8')) {
        $content = utf8_encode($content);
    }
$content = preg_replace("/[\n\r]/","",$content);
    $content = trim(clean_description($content));
    return $content;
}

function clean_url($content,$replace_optr = '-') {
        $content = html_entity_decode($content);
        $content = strip_tags($content);
        if (!mb_check_encoding($content, 'UTF-8')) {
            $content = utf8_encode($content);
        }
        $content = trim($content);
        $content = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $content); // Removes special chars.
        $content = preg_replace('!\s+!', $replace_optr , $content);
        return strtolower($content);
    }

function prepare_index_data($content) {
    $content = str_replace('&', ' ', $content);
    $content = str_replace(':', ' ', $content);
    $content = str_replace('-', ' ', $content);
    $content = html_entity_decode($content);
    $content = strip_tags($content);
    if (!mb_check_encoding($content, 'UTF-8')) {
        $content = utf8_encode($content);
    }
    $content = trim($content);
    $content = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $content); // Removes special chars.
    $content = preg_replace('!\s+!', ' ', $content);
    $content = str_replace(' ','-',$content);	
    return $content;
}
?>
