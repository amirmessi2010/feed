<?php
require_once 'ftp.php';
define('DELIVERY_TIME', 3);
define('PAGE_SIZE',100000);
define('PRODUCTION',true);
header('Content-Type: text/xml'); 

class SimpleXMLExtended extends SimpleXMLElement {
  public function addCData($cdata_text) {
    $node = dom_import_simplexml($this); 
    $no   = $node->ownerDocument; 
    $node->appendChild($no->createCDATASection($cdata_text)); 
  } 
}

function pageUrl(){
    /*if(isset($_GET['page']))
        unset($_GET['page']);
    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } 
    else 
    {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }*/
    $pageURL = 'https://www.cart2india.com/';
    return $pageURL;
}

function removeQuerystringVar($url) {
    $url_arr = explode('?', $url);
    return $url_arr[0];
}
$current_page = 1;
$pg_url = removeQuerystringVar(pageUrl());



$production = PRODUCTION;
if ($production) {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'c2iadmin', 'C@rt2Indi@!23DB', 'pm');
} else {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'root', '', 'pm_new');
}

function clean_title($content) {
    $content = html_entity_decode($content);
    $content = strip_tags($content);
    if (!mb_check_encoding($content, 'UTF-8')) {
        $content = utf8_encode($content);
    }
    $content=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $content);
    $content = trim($content);
    return $content;
}

function prepare_index_data($content) {
    $content = str_replace('&', ' ', $content);
    $content = str_replace('and', ' ', $content);
    $content = str_replace(':', ' ', $content);
    $content = str_replace('-', ' ', $content);
    $content = html_entity_decode($content);
    $content = strip_tags($content);
    if (!mb_check_encoding($content, 'UTF-8')) {
        $content = utf8_encode($content);
    }
    $content = trim($content);
    $content = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $content); // Removes special chars.
    $content = preg_replace('!\s+!', ' ', $content);
    return $content;
}

function replaceExtraString($content){
    return strip_tags(str_replace('&', ' ', $content));
}
function cleanup_text($content,$replace_optr = '-') {
        $content = html_entity_decode($content);
        $content = strip_tags($content);
        if (!mb_check_encoding($content, 'UTF-8')) {
            $content = utf8_encode($content);
        }
        $content = trim($content);
        $content = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $content); // Removes special chars.
        $content = preg_replace('!\s+!', $replace_optr , $content);
        $content=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $content);
        return strtolower($content);
    }
function WriteSiteMapData($data) {
        $fileName = "sitemap.xml";
        $log_path = getcwd(); //log folder
        $fp = fopen($log_path .DIRECTORY_SEPARATOR. $fileName, 'a+');
        fwrite($fp, $data . "\n");
        fclose($fp);
    }    
    
function getProductCount(){
    global $mysqli ;
    //total query
        $cquery = ' SELECT COUNT(*) as total 
                    FROM merchant_products_master p 
                    INNER JOIN categories c ON p.`category_id`=c.id  
                    WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0 AND  p.priority > 0 ';
        $tresult = $mysqli->query($cquery);
        $tdata = $tresult->fetch_object();
        return $tdata->total;
        
}    

function getPages(){
    global $limit ;
    $total = getProductCount();
    
    return (int)($total/$limit) + 1;
}
function generateXml($xml) {
    global $mysqli,$offset,$limit,$pg_url,$current_page,$domain ;
    
        $meta_include_arr = array('Brand' , 'Images1', 'Images2', 'Images3', 'EditorialReview1', 'ASIN');
        
        $meta_include_arr_map = array(
            'Brand' => 'brand', 
            'Images1' => 'image', 
            'Images2' => 'other_image_url1', 
            'Images3' => 'other_image_url2',
            'EditorialReview1' => 'description',
            'ASIN' => 'product_id_type'
            );
        
        $query = 'SELECT 
                    p.india_price, p.availability_flag, 
                    IF(p.merchant_id = 1,p.merchant_sku,"") as merchant_sku, p.category_id, c.name, p.Title, 
                    p.priority, p.india_price offer_price, 
                    p.uae_price, p.c2i_sku,
                    r.rbn
                  FROM merchant_products_master p 
                  INNER JOIN categories c ON p.`category_id`=c.id 
                  LEFT JOIN rbn r ON r.id = c.id 
                  WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0 AND  p.priority > 0 ';
        
        $query .= ' LIMIT '.$offset. ', '.$limit;
        
        $result = $mysqli->query($query);
        
        //generate xml string
        
        $items = $xml->addChild('itemlist');
        
        while ($row = $result->fetch_object()) {
            
            $meta_query = 'SELECT meta_key, meta_value FROM merchant_product_meta WHERE c2i_sku=' . $row->c2i_sku;
            $meta_result = $mysqli->query($meta_query);
            $title = cleanup_text($row->Title);
            $category_name = cleanup_text($row->name);
            $url = $domain .'/'. $category_name.'/'.$title.'/' . $row->c2i_sku . '/';
            $product = array(
                    'sku' => ltrim($row->c2i_sku,'0'),
                    'title' => utf8_encode($row->Title),
                    'availability' => utf8_encode($row->availability_flag),
                    'link' => utf8_encode($url),
                    'india_price' => $row->india_price,
                    'delivery_time' => DELIVERY_TIME,
                    'category_id' => $row->category_id,
                    'rbn' => empty($row->rbn)?' ':$row->rbn,
                    'asin' => utf8_encode($row->merchant_sku),
                    'standard_product_id' => utf8_encode($row->merchant_sku),
                    'offer_note' => utf8_encode("Brand New, Genuine Import - Ships from USA - COD available")
                );
            
            //get meta data
            while ($meta = $meta_result->fetch_object()){
                if(in_array(trim($meta->meta_key), $meta_include_arr)){
                    $product[$meta_include_arr_map[trim($meta->meta_key)]]= trim(utf8_encode(replaceExtraString($meta->meta_value)));
                }
            }
            
            
            // assigned to xml object 
            $item = $items->addChild('item');
            foreach($product as $key => $value){
//                if(!in_array($key, array('sku','availability','link','india_price','delivery_time','category_id','rbn','asin','standard_product_id','offer_note')))
                
                if(in_array(strtolower(trim($key)), array('title'))){
                    $item->$key = NULL; // VERY IMPORTANT! We need a node where to append
                    $item->$key->addCData($value);
                }
                else {
                    $item->addChild($key, $value);
                }        
                
//                $child = $item->addChild($key);
//                $child->value = $value;
            } 
            
            unset($product);
        }
        
        return $xml ;
}
$pages = 1;

$limit = PAGE_SIZE;
$pages = getPages();
for($i=0;$i<$pages;$i++){
    $xml = new SimpleXMLExtended('<xml/>');
    $offset = $i * $limit;
    $xml =  generateXml($xml);
    $abso_path = getcwd().'/';
    $filename = $abso_path.'data/'.'product'.$i.'.xml';
    $xml->saveXML($filename);
    
    $destination_file = 'product'.$i.'.xml'; //where you want to throw the file on the webserver (relative to your login dir)
    ftpTransfer($filename,$destination_file); 
    
    unset($xml);
}

?>

