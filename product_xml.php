<?php
define('DELIVERY_TIME', 3);
define('PAGE_SIZE',1000);
define('PRODUCTION',true);
header('Content-Type: text/xml'); 


function pageUrl(){
    if(isset($_GET['page']))
        unset($_GET['page']);
    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } 
    else 
    {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function removeQuerystringVar($url) {
    $url_arr = explode('?', $url);
    return $url_arr[0];
}
$current_page = isset($_GET['page'])?$_GET['page']:1;
$pg_url = removeQuerystringVar(pageUrl());


$limit = PAGE_SIZE;
$offset = ($current_page - 1) * $limit;
$production = PRODUCTION;
if ($production) {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'c2iadmin', 'C@rt2Indi@!23DB', 'pm');
} else {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'root', '', 'pm_new');
}

function clean_title($content) {
    $content = html_entity_decode($content);
    $content = strip_tags($content);
    if (!mb_check_encoding($content, 'UTF-8')) {
        $content = utf8_encode($content);
    }
    $content=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $content);
    $content = trim($content);
    return $content;
}

function prepare_index_data($content) {
    $content = str_replace('&', ' ', $content);
    $content = str_replace('and', ' ', $content);
    $content = str_replace(':', ' ', $content);
    $content = str_replace('-', ' ', $content);
    $content = html_entity_decode($content);
    $content = strip_tags($content);
    if (!mb_check_encoding($content, 'UTF-8')) {
        $content = utf8_encode($content);
    }
    $content = trim($content);
    $content = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $content); // Removes special chars.
    $content = preg_replace('!\s+!', ' ', $content);
    return $content;
}

function replaceExtraString($content){
    return strip_tags(str_replace('&', ' ', $content));
}
function cleanup_text($content,$replace_optr = '-') {
        $content = html_entity_decode($content);
        $content = strip_tags($content);
        if (!mb_check_encoding($content, 'UTF-8')) {
            $content = utf8_encode($content);
        }
        $content = trim($content);
        $content = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $content); // Removes special chars.
        $content = preg_replace('!\s+!', $replace_optr , $content);
        $content=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $content);
        return strtolower($content);
    }
function WriteSiteMapData($data) {
        $fileName = "sitemap.xml";
        $log_path = getcwd(); //log folder
        $fp = fopen($log_path .DIRECTORY_SEPARATOR. $fileName, 'a+');
        fwrite($fp, $data . "\n");
        fclose($fp);
    }    
function generateXml($xml) {
    global $mysqli,$offset,$limit,$pg_url,$current_page,$domain ;
    
        $meta_include_arr = array('Brand' , 'Images1', 'Images2', 'Images3', 'EditorialReview1', 'ASIN');
        
        $meta_include_arr_map = array(
            'Brand' => 'brand', 
            'Images1' => 'image', 
            'Images2' => 'other_image_url1', 
            'Images3' => 'other_image_url2',
            'EditorialReview1' => 'description',
            'ASIN' => 'product_id_type'
            );
        
        $query = 'SELECT 
                    p.india_price, p.availability_flag, 
                    IF(p.merchant_id = 1,p.merchant_sku,"") as merchant_sku, p.category_id, c.name, p.Title, 
                    p.priority, p.india_price offer_price, 
                    p.uae_price, p.c2i_sku, r.rbn  
                  FROM merchant_products_master p 
                  INNER JOIN categories c ON p.`category_id`=c.id 
                  LEFT JOIN rbn r ON r.id = c.id 
                  WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0 AND  p.priority > 0 ';
        
        $query .= ' LIMIT '.$offset. ', '.$limit;
        $result = $mysqli->query($query);
        
        //total query
        $cquery = ' SELECT COUNT(*) as total 
                    FROM merchant_products_master p 
                    INNER JOIN categories c ON p.`category_id`=c.id  
                    WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0 AND  p.priority > 0 ';
        $tresult = $mysqli->query($cquery);
        $tdata = $tresult->fetch_object();
        
        $pages = (int)($tdata->total/$limit) + 1; 
        
        //generate xml string
        
        $items = $xml->addChild('itemlist');
        while ($row = $result->fetch_object()) {
            
            $meta_query = 'SELECT meta_key, meta_value FROM merchant_product_meta WHERE c2i_sku=' . $row->c2i_sku;
            $meta_result = $mysqli->query($meta_query);
            $title = cleanup_text($row->Title);
            $category_name = cleanup_text($row->name);
            $url = $domain .'/'. $category_name.'/'.$title.'/' . $row->c2i_sku . '/';
            $product = array(
                    'sku' => ltrim($row->c2i_sku,'0'),
                    'title' => $row->Title,
                    'availability' => $row->availability_flag,
                    'link' => $url,
                    'india_price' => $row->india_price,
                    'delivery_time' => DELIVERY_TIME,
                    'category_id' => $row->category_id,
                    'rbn' => empty($row->rbn)?' ':$row->rbn,
                    'asin' => $row->merchant_sku,
                    'standard_product_id' => $row->merchant_sku,
                    'offer_note' => "Brand New, Genuine Import - Ships from USA - COD available"
                );
            
            //get meta data
            while ($meta = $meta_result->fetch_object()){
                if(in_array(trim($meta->meta_key), $meta_include_arr)){
                    $product[$meta_include_arr_map[trim($meta->meta_key)]]= trim(replaceExtraString($meta->meta_value));
                }
            }
            
            
            // assigned to xml object 
            $item = $items->addChild('item');
            foreach($product as $key => $value){
                $item->addChild($key, $value);
            } 
            
            unset($product);
        }
        
        if($pages > $current_page){
            
            $next_page_url = $items->addChild('next_page_url');
            $next_page_url->addChild('url', $pg_url.'?page=' . ($current_page + 1));
        }
            
        
        
        return $xml ;
}

/*function generateXmlBck() {
    global $mysqli,$offset,$limit,$url,$current_page;
    $query = 'SELECT p.category_id, p.category_flag, p.created, p.category_id, c.name, p.Title, p.priority, p.india_price offer_price, p.uae_price, p.c2i_sku, pm.`meta_value` FROM merchant_products_master p INNER JOIN categories c ON p.`category_id`=c.id  INNER JOIN merchant_product_meta pm ON p.`c2i_sku`=pm.`c2i_sku` AND pm.`meta_key`="Images1" WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0';
    $query .= ' LIMIT '.$offset. ', '.$limit;
    //total query
    $cquery = 'SELECT COUNT(*) as total FROM merchant_products_master p WHERE p.availability_flag = 1 AND p.india_price > 0 ';
    
    $tresult = $mysqli->query($cquery);
    
    $tdata = $tresult->fetch_object();
    
    $result = $mysqli->query($query);
    
    while ($row = $result->fetch_object()) {
        $brand_query = 'SELECT meta_value FROM merchant_product_meta WHERE meta_key="brand" AND c2i_sku=' . $row->c2i_sku;
        $brand_result = $mysqli->query($brand_query);
        $brand = $brand_result->fetch_object();
        if (isset($brand->meta_value))
            $brand = prepare_index_data($brand->meta_value);
        else
            $brand = '';
        $title = clean_title($row->Title);
        $search_title = prepare_index_data($row->Title);
        $url = $domain . '/product/productDetails/' . $row->c2i_sku . '/';
        $category = prepare_index_data($row->name);
        $echo .= 
              '<item>
                <sku> '.$data->c2i_sku.'</sku>
                <title>'. $data->Title.'</title>
                <link>'. $data->india_price.'</link>
                <price>'. $data->india_price.'</price>
                <price>'. $data->india_price.'</price>
                <price>'. $data->india_price.'</price>    
              </item>';
        $product_document = new \Elastica\Document($row->c2i_sku, $product);
        $elasticaType->addDocument($product_document);
        $elasticaType->getIndex()->refresh();
        unset($product);
        unset($product_document);
        unset($row);
    }
    $pages = (int)($tdata->total/$limit) + 1;
    $echo = '<xml version="1.0">';
    $echo .= '<items>';
    while ($data = $cresult->fetch_object()) {
        $current_cat = $this->cleanup_text($data->category_name);
        $echo .= 
              '<item>
                <sku> '.$data->c2i_sku.'</sku>
                <title>'. $data->Title.'</title>
                <link>'. $data->india_price.'</link>
                <price>'. $data->india_price.'</price>
                <price>'. $data->india_price.'</price>
                <price>'. $data->india_price.'</price>    
              </item>';
        
    }
    
    if($pages > $current_page)
        $echo .= '<next_page_url>'.$url.'?page=' . ($current_page + 1).'</next_page_url>';
    $echo .= '</items>';
    $echo .= '</xml>';
    
    return $echo;
}*/


$xml = new SimpleXMLElement('<xml/>');
$xml =  generateXml($xml);
print($xml->asXML());
$xml->saveXML('sitemap.xml');
?>

