<?php
require_once 'ftp.php';
define('DELIVERY_TIME', 3);
define('PAGE_SIZE',50000);
define('PRODUCTION',true);
header('Content-Type: text/xml'); 

class SimpleXMLExtended extends SimpleXMLElement {
  public function addCData($cdata_text) {
    $node = dom_import_simplexml($this); 
    $no   = $node->ownerDocument; 
    $node->appendChild($no->createCDATASection($cdata_text)); 
  } 
}

function pageUrl(){
    /*if(isset($_GET['page']))
        unset($_GET['page']);
    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } 
    else 
    {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }*/
    $pageURL = 'https://www.cart2india.com/';
    return $pageURL;
}

function removeQuerystringVar($url) {
    $url_arr = explode('?', $url);
    return $url_arr[0];
}
$current_page = 1;
$pg_url = removeQuerystringVar(pageUrl());



$production = PRODUCTION;
if ($production) {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'c2iadmin', 'C@rt2Indi@!23DB', 'pm');
} else {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'root', '', 'pm_new');
}

function clean_title($content) {
    $content = html_entity_decode($content);
    $content = strip_tags($content);
    if (!mb_check_encoding($content, 'UTF-8')) {
        $content = utf8_encode($content);
    }
    $content=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $content);
    $content = trim($content);
    return $content;
}

function prepare_index_data($content) {
    $content = str_replace('&', ' ', $content);
    $content = str_replace('and', ' ', $content);
    $content = str_replace(':', ' ', $content);
    $content = str_replace('-', ' ', $content);
    $content = html_entity_decode($content);
    $content = strip_tags($content);
    if (!mb_check_encoding($content, 'UTF-8')) {
        $content = utf8_encode($content);
    }
    $content = trim($content);
    $content = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $content); // Removes special chars.
    $content = preg_replace('!\s+!', ' ', $content);
    return $content;
}

function replaceExtraString($content){
    return strip_tags(str_replace('&', ' ', $content));
}
function cleanup_text($content,$replace_optr = '-') {
        $content = html_entity_decode($content);
        $content = strip_tags($content);
        if (!mb_check_encoding($content, 'UTF-8')) {
            $content = utf8_encode($content);
        }
        $content = trim($content);
        $content = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $content); // Removes special chars.
        $content = preg_replace('!\s+!', $replace_optr , $content);
        $content=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $content);
        return strtolower($content);
    }
function WriteSiteMapData($data) {
        $fileName = "sitemap.xml";
        $log_path = getcwd(); //log folder
        $fp = fopen($log_path .DIRECTORY_SEPARATOR. $fileName, 'a+');
        fwrite($fp, $data . "\n");
        fclose($fp);
    }    
    
function getProductCount(){
    global $mysqli ;
    //total query
        $cquery = ' SELECT COUNT(*) as total 
                    FROM merchant_products_master p 
                    INNER JOIN categories c ON p.`category_id`=c.id  
                    WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0 AND  p.priority >= 6 ';
        $tresult = $mysqli->query($cquery);
        $tdata = $tresult->fetch_object();
        return $tdata->total;
        
}    

function getPages(){
    global $limit ;
    $total = getProductCount();
    
    return (int)($total/$limit) + 1;
}

function getCategoryName($category_id){
    global $mysqli ;
    $query = sprintf('SELECT c.name as category_name FROM categories c WHERE id = %d',$category_id);
    $result = $mysqli->query($query);
    
    if($result->num_rows == 0)
        return false;
    
    $row = $result->fetch_object();
    
    return $row->category_name;
}

function generateXml($xml) {
    global $mysqli,$offset,$limit,$pg_url,$current_page,$domain ;
    
    
        $meta_include_arr = array('Brand' , 'Images1', 'EditorialReview1');
        
        $meta_include_arr_map = array(
            'Brand' => 'Brand', 
            'Images1' => 'Prod_Image',             
            'EditorialReview1' => 'Description',
            'ASIN' => 'product_id_type'
            );
        
         $query = 'SELECT 
                    p.india_price, p.availability_flag, 
                    IF(p.merchant_id = 1,p.merchant_sku,"") as merchant_sku,
                    p.category_id, 
                    c.name as subsub_category, 
                    p.Title, 
                    c.category_head, 
                    c.parent_id, 
                    p.priority, 
                    p.india_price offer_price, 
                    p.uae_price, 
                    p.c2i_sku, 
                    p.shipping_cost 
                  FROM merchant_products_master p 
                  INNER JOIN categories c ON p.`category_id`=c.id 
                  WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0 AND  p.priority > 6 AND c.category_head != 600000 ORDER BY p.priority DESC  ';
        
        $query .= ' LIMIT '.$offset. ', '.$limit;
        
        $result = $mysqli->query($query);
        
        if($result->num_rows == 0)
            return false;
        
        //generate xml string
        while ($row = $result->fetch_object()) {
            
            $meta_query = 'SELECT meta_key, meta_value FROM merchant_product_meta WHERE c2i_sku=' . $row->c2i_sku;
            $meta_result = $mysqli->query($meta_query);
            $title = cleanup_text($row->Title);
            $category_name = cleanup_text($row->subsub_category);
            $url = $domain .'/'. $category_name.'/'.$title.'/' . $row->c2i_sku ;
            $product = array(
                    'Product_Name' => iconv(mb_detect_encoding($row->Title, mb_detect_order(), true), "UTF-8", $row->Title),
                    'Availability' =>iconv(mb_detect_encoding($row->availability_flag, mb_detect_order(), true), "UTF-8", $row->availability_flag),
                    'Actual_Price' => intval(ceil($row->india_price)),
                    'Discount_Price' => intval(ceil($row->india_price)),
                    'URL' => $url,
                    'Shipping_Price' => intval(ceil($row->shipping_cost)),
                    'Store_Name' => 'Cart2India', 
                    'Shipping_Days' => '6 - 10 business days', 
                );
            
                $category_head = getCategoryName($row->category_head);
                $product['Category_Name'] = $category_head;
                
                $parent_category = getCategoryName($row->parent_id);
                $product['SubCategory_Name'] = $parent_category;
                $product['SubSubCategory_Name'] = utf8_encode($row->subsub_category);                
            
            //get meta data
            while ($meta = $meta_result->fetch_object()){
                
                if(in_array(trim($meta->meta_key), $meta_include_arr)){
                    $product[$meta_include_arr_map[trim($meta->meta_key)]]= trim(utf8_encode(replaceExtraString($meta->meta_value)));
                }
            }
            
            //reorder the array
            $ordered_product = array();
            $ordered_product = array(
                'Product_Name' => $product['Product_Name'],
                'Description' => isset($product['Description'])?$product['Description']:null,
                'Actual_Price' => $product['Actual_Price'],
                'Discount_Price' => $product['Discount_Price'],
                'URL' => $product['URL'],
                'Shipping_Price' => $product['Shipping_Price'],
                'Availability' => $product['Availability'],
                'Brand' => $product['Brand'],
                );
            if(isset($product['Category_Name']))
                $ordered_product['Category_Name'] = $product['Category_Name'];
            if(isset($product['SubCategory_Name']))
                $ordered_product['SubCategory_Name'] = $product['SubCategory_Name'];
            if(isset($product['SubSubCategory_Name']))
                $ordered_product['SubSubCategory_Name'] = $product['SubSubCategory_Name'];
            
            $ordered_product['Store_Name']  = $product['Store_Name'];
            $ordered_product['Prod_Image']  = $product['Prod_Image'];
            $ordered_product['Shipping_Days']  = $product['Shipping_Days'];
            $ordered_product['COD']  = 'Yes'; 
            // assigned to xml object 
            $item = $xml->addChild('Item');
            foreach($ordered_product as $key => $value){
//                if(!in_array($key, array('sku','availability','link','india_price','delivery_time','category_id','rbn','asin','standard_product_id','offer_note')))
                
                if(in_array(strtolower(trim($key)), array('product_name','description'))){
                    $item->$key = NULL; // VERY IMPORTANT! We need a node where to append
                    $item->$key->addCData($value);
                }
                else {
                    $item->addChild($key, $value);
                }        
                
//                $child = $item->addChild($key);
//                $child->value = $value;
            } 
            
            unset($product);
        }
        
        return $xml ;
}
$pages = 1;

$limit = PAGE_SIZE;
$pages = getPages();
for($i=0;$i<$pages;$i++){
    $xml = new SimpleXMLExtended('<channel/>');
    $offset = $i * $limit;
    $xml =  generateXml($xml);
    
    $abso_path = getcwd().'/';
//    $abso_path = null;
    $filename = $abso_path.'data/'.'buyhatke'.$i.'.xml';
    $xml->saveXML($filename);
    
    $desination_folder = 'buyhatke';
    $destination_file = $desination_folder.'/buyhatke'.$i.'.xml'; //where you want to throw the file on the webserver (relative to your login dir)
    ftpTransfer($filename,$destination_file,$desination_folder); 
        
    unset($xml);
}

?>

