<?php
ini_set('memory_limit', '5048M');
ini_set('max_execution_time', 30000000);
require_once 'utility.php';
require_once 'ftp.php';
define('DELIVERY_TIME', 3);
define('PAGE_SIZE',1000);
define('PRODUCTION',true);
define('NO_OF_FILES',40);
define('LIMIT',50000);

function fputcsv_eol($fp, $array, $delimeter, $encloser, $eol) {
  fputcsv($fp, $array,$delimeter,$encloser);
  if("\n" != $eol && 0 === fseek($fp, -1, SEEK_CUR)) {
    fwrite($fp, $eol);
  }
}
class Customer {
    
    public $mode;
    public $db;
    public $limit = 10000;
    public $domain;

    public function __construct() {
        $this->mode = PRODUCTION;
        if ($this->mode) {
            $this->domain = 'https://www.cart2india.com';

            $this->db = new mysqli('localhost', 'c2iadmin', 'C@rt2Indi@!23DB', 'pm');
        } else {
            $this->domain = 'https://www.cart2india.com';
            $this->db = new mysqli('localhost', 'root', '', 'c2iportal_test');
        }
    }
    
    public function generate(){

        
            $query = 'SELECT c.first_name, c.last_name, c.email 
                  FROM customer c ';

            $result = $this->db->query($query);

            if($result->num_rows == 0){
                break;
            }
            $abso_path = '/home/c2iweb/feed/data/';            
             

            $file_name = 'customer.csv';
            $source_file = $abso_path.$file_name;
            $fp = fopen($source_file, 'w');
            
            $header = array('First Name','Last Name','Email');
            fputcsv($fp, $header);

            while($row = $result->fetch_assoc()){
                $_data = array();
                $_data['first_name'] = trim($row['first_name']);
                $_data['last_name'] = trim($row['last_name']);
                $_data['email'] = trim($row['email']);

                fputcsv($fp, $_data);
            }  

            fclose($fp);  
        
    }
}

$product = new Customer();
$product->generate();

class CustomerGulf {
    
    public $mode;
    public $db;
    public $limit = 10000;
    public $domain;

    public function __construct() {
        $this->mode = PRODUCTION;
        if ($this->mode) {
            $this->domain = 'https://www.cart2india.com';

            $this->db = new mysqli('localhost', 'root', 'c@rt@indi@DB', 'c2gweb');
        } else {
            $this->domain = 'https://www.cart2india.com';
            $this->db = new mysqli('localhost', 'root', '', 'c2iportal_test');
        }
    }
    
    public function generate(){

        
            $query = 'SELECT c.first_name, c.last_name, c.email 
                  FROM customer c ';

            $result = $this->db->query($query);

            if($result->num_rows == 0){
                break;
            }
            $abso_path = '/home/c2iweb/feed/data/';            
             

            $file_name = 'customer_gulf.csv';
            $source_file = $abso_path.$file_name;
            $fp = fopen($source_file, 'w');
            
            $header = array('First Name','Last Name','Email');
            fputcsv($fp, $header);

            while($row = $result->fetch_assoc()){
                $_data = array();
                $_data['first_name'] = trim($row['first_name']);
                $_data['last_name'] = trim($row['last_name']);
                $_data['email'] = trim($row['email']);

                fputcsv($fp, $_data);
            }  

            fclose($fp);  
        
    }
}

$product = new CustomerGulf();
$product->generate();
?>

