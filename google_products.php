<?php
ini_set('memory_limit', '5048M');
ini_set('max_execution_time', 30000000);
require_once 'utility.php';
require_once 'ftp.php';
define('DELIVERY_TIME', 3);
define('PAGE_SIZE',1000);
define('PRODUCTION',true);


class GoogleProducts {
    
    public $mode;
    public $db;
    public $limit = 10000;
    public $domain;

    public function __construct() {
        $this->mode = PRODUCTION;
        if ($this->mode) {
            $this->domain = 'https://www.cart2india.com';

            $this->db = new mysqli('localhost', 'c2iadmin', 'C@rt2Indi@!23DB', 'pm');
        } else {
            $this->domain = 'https://www.cart2india.com';
            $this->db = new mysqli('localhost', 'root', '', 'pm_new');
        }
    }
    private function _generate($category_id,$category,&$fp,$file_name){
        $abso_path = 'data/';
        $filter_array = array('Images1','EditorialReview1','Brand');
        
        $map_array = array(
            'Images1'=>'image_link',
            'EditorialReview1'=>'description',
            'Brand'=>'brand',
        );
        
        $sqlQuery = sprintf("
            SELECT p.c2i_sku as c2i_sku, p.Title as title, p.india_price as india_price, p.weight , p.availability_flag as availability, c.name, p.category_id  
            FROM merchant_products_master p 
            JOIN categories c ON p.category_id = c.id 
            WHERE c.category_head = %d AND p.india_price > 0 AND p.availability_flag = 1  and p.priority > 0  
            ORDER BY p.priority DESC 
            LIMIT %d
        ",$category_id,  $this->limit);
        
        $result = $this->db->query($sqlQuery);
        
        if($result->num_rows){
            $data = array();
            
            
            while ($row = $result->fetch_assoc()){                
                                
                $meta_query = sprintf('SELECT * FROM merchant_product_meta WHERE c2i_sku = %d ',$row['c2i_sku']);
                
                $result_meta = $this->db->query($meta_query);
                $_data = array();
                if($result_meta->num_rows){
                    $meta = array();
                    while($row_meta = $result_meta->fetch_assoc()){ 
                        if(in_array($row_meta['meta_key'], $filter_array)){                            
                            $meta[$map_array[$row_meta['meta_key']]] = clean_title(strip_tags($row_meta['meta_value']));
                        }
                        
                    }
                }
                
                //data 
                $_data['id'] = ltrim($row['c2i_sku'],0);
                $_data['title'] = $row['title'];
                $_data['description'] = isset($meta['description'])?$meta['description']:"no description";                
                $_data['link'] = $this->getUrl($row['name'],$row['title'],$row['c2i_sku']);
                $_data['price'] = sprintf("%d INR",$row['india_price']);
                $_data['brand'] = isset($meta['brand'])?$meta['brand']:"generic";
                $_data['condition'] = "new";
                $_data['image_link'] = isset($meta['image_link'])?$meta['image_link']:'no imagelink';
                $_data['weight'] = sprintf("%d lbs",$row['weight']);
                $_data['product_type'] = $this->getCategoryTree($row['category_id']);
                $_data['quantity'] = 50;
                $_data['availability'] = ($row['availability'] == true)?"in stock":"out of stock";
                $_data['google_product_category'] = end($category);
                
                array_walk($_data,function(&$item){
                    return clean_title($item);
                });
			
                fputcsv($fp, $_data,"\t",chr(0));
                
            }
            
            
            $abso_path = '/home/c2iweb/feed/data/';
            $source_file = $abso_path.$file_name;
            ftpTransfer($source_file,$file_name);  
//            $record[] = implode('/t', $header);
//            $record[] = implode('/n', $data);
//            
//            $total_records  = implode('/n',$record);
//            $file_location = $category[0].'.txt';
//            file_put_contents($file_location, $total_records);
        }
        
    }
    
  
    
    public function refArray(){
        return array(
            100000 => array('csa','Apparel & Accessories > Clothing Accessories'),
            500000 => array('baby','Baby & Toddler'),
            90000000 => array('camera','Cameras & Optics'),
            1100000 => array('computers','Electronics > Computers'),
            1200000 => array('electronics','Electronics'),
            17000000 => array('homekitchen','Home & Garden'),
            2000000 => array('sports','Sporting Goods'),
            2200000 => array('pets','Animals & Pet Supplies > Pet Supplies'),
            8010000 => array('beauty','Health & Beauty > Personal Care > Cosmetics'),
            600000 => array('cellphone','Electronics > Communications > Telephony > Mobile Phone Accessories'),
            8020000 => array('healthpersonalcare','Health & Beauty > Health Care'),
            1400000 => array('musical','Arts & Entertainment > Hobbies & Creative Arts > Musical Instrument Accessories'),
            2100000 => array('toys','Toys & Games > Toys > Building Toys'),

        );
    }
    
    public function getHeaders(){
        return array(
            'id','title','description','link','price','brand','condition','image link','weight','product type','quantity','availability','google product category'
        );
    }
    //************************************************************************
    public function getParentNodes($category_id, &$leaves) {
        $parent_nodes = array();
        if ($this->hasParent($category_id, $parent_nodes)) {

            $leaves[] = $parent_nodes;
            $this->getParentNodes($parent_nodes['parent_id'], $leaves);
        } else {
//            if (! in_array($category_id, $leaves) && empty($leaves) && is_array($leaves))
//                $leaves[] = $category_id;
        }
    }

    public function getUrl($category_name,$title,$c2i_sku){
        $cat_name = str_replace("'", '-', clean_url($category_name));
        $pro_name = str_replace("'", '-', clean_url($title));
        return $this->domain . sprintf('/%s/%s/',$cat_name,$pro_name) . $c2i_sku;
    }
    //************************************************************************
    public function hasParent($category_id, &$parent_nodes) {

        $sqlQuery = "SELECT
                        id,
                        name,
                        parent_id
                    FROM 
                        categories
                    WHERE 
                        id = $category_id LIMIT 1";

        $result = $this->db->query($sqlQuery);
        
        if($result->num_rows){
            $categories[] = $result->fetch_assoc();
            
            
            $parent_nodes = array_shift($categories);
            
            return true;
        }
        
        return false;
    }
    
    
    
    public function getCategoryTree($category_id){
        $parent_nodes = array();
        $this->getParentNodes($category_id,$parent_nodes);
        
        $tree = null;
        foreach($parent_nodes as $node){
            $tree .= $node['name'].'>';
        }
        
        return $tree;
    }




    public function generate(){
        $categories = $this->refArray();
        
        foreach($categories as $category_id => $category){
            $header = $this->getHeaders();
//            $file_name = $category[0].'.txt';
            $abso_path = getcwd().'/';
            $file_name = $abso_path.'data/'.$category[0].'.txt';
            $fp = fopen($file_name, 'w');
            fputcsv($fp, $header,"\t",chr(0));
            $this->_generate($category_id,$category,$fp,$file_name);
            fclose($fp);
        }
    }    
    
    public function transfer(){
        $ref_array = $this->refArray();
        foreach($ref_array as $category){
            $file_name = $category[0].'.txt';
            $abso_path = '/home/c2iweb/feed/data/';
            $source_file = $abso_path.$file_name;
            $desination_folder = 'google';
//            $destination_file = $desination_folder.'/'.$file_name;
            ftpTransfer($source_file,$file_name);
        }
    }
}

$product = new GoogleProducts();
$product->generate();
$product->transfer();
?>

