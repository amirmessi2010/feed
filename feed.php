<?php header('Content-Type: text/xml'); 
function pageUrl(){
    if(isset($_GET['page']))
        unset($_GET['page']);
    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } 
    else 
    {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}
function removeQuerystringVar($url) {
    $url_arr = explode('?', $url);
    return $url_arr[0];
}
$current_page = isset($_GET['page'])?$_GET['page']:1;
$url = removeQuerystringVar(pageUrl());


$limit = 5000;
$offset = ($current_page - 1) * $limit;
$production = 1;
if ($production) {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'c2iadmin', 'C@rt2Indi@!23DB', 'pm');
} else {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'root', '', 'pm_new');
}



function generateXml() {
    global $mysqli,$offset,$limit,$url,$current_page;
    $query = 'SELECT p.india_price, p.c2i_sku FROM merchant_products_master p WHERE p.availability_flag = 1 AND p.india_price > 0 ';
    $query .= 'LIMIT '.$offset. ', '.$limit;
    $cquery = 'SELECT COUNT(*) as total FROM merchant_products_master p WHERE p.availability_flag = 1 AND p.india_price > 0 ';
    $cresult = $mysqli->query($query);
    $tresult = $mysqli->query($cquery);
    
    $tdata = $tresult->fetch_object();
    
    $pages = (int)($tdata->total/$limit) + 1;
    $echo = '<xml version="1.0">';
    $echo .= '<items>';
    while ($data = $cresult->fetch_object()) {
        $echo .= 
              '<item>
                <sku> '.$data->c2i_sku.'</sku>
                <price>'. $data->india_price.'</price>
              </item>';
        
    }
    
    if($pages > $current_page)
        $echo .= '<next_page_url>'.$url.'?page=' . ($current_page + 1).'</next_page_url>';
    $echo .= '</items>';
    $echo .= '</xml>';
    
    return $echo;
}



echo generateXml();
?>

