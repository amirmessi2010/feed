<?php 
error_reporting(E_ALL);
$production = 1;
if ($production) {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'root', 'c@rt@indi@DB', 'c2iweb');
} else {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'root', '', 'c2iportal_test');
}
$char_set = 'utf8';
mysqli_set_charset($char_set);
class OrderInsert{
    public $report = array();    
    
    public function insertData(){
        global $mysqli;
        $request_data = $_POST;
        
        $credentials = $request_data['credentials'];
        $type = $request_data['type']; // for future use
        
        $cquery = sprintf('SELECT * FROM pg_clients pc WHERE  username = "%s" AND password = "%s"',$credentials['username'],$credentials['password']);
        
        $cresult = $mysqli->query($cquery);
        
        if($cresult->num_rows != 1){
            $this->report['status'] = false;
            $this->report['message'] = 'Incorrect credential';
            echo json_encode($this->report);    
            die;
        }
        else {
            $data = array();
                $total_query  = 
                "SELECT o.order_id, 
                    op.op_id, op.description, op.quantity,  op.unit_price, op.shipping_price,o.discount_amount,o.discount_type, op.c2i_sku, 
                    a.first_name ,a.last_name, c.email, a.gender, a.company, a.address_1, a.address_2, a.state, a.city, 
                    a.post_code, co.name, a.mobile, a.landline , t.card_type as payment_type, t.authorize_transaction_id as authorize_transaction_id 
                FROM  `orders` AS o
                JOIN order_products AS op ON o.order_id = op.order_id
                JOIN address AS a ON a.address_id = o.shipping_address_id
                JOIN customer AS c ON c.id = o.customer_id
                JOIN country AS co ON a.country_id = co.country_id   
                LEFT JOIN transaction AS t ON t.payment_transaction_id = op.payment_transaction_id 
                "; 
                
                $total_query .= " WHERE op.status_id = 1 ";
                $total_query .= " order by op.op_id desc ";
                
                $cresult = $mysqli->query($total_query);
                while ($order_product = $cresult->fetch_object()){
                    $data[] = $order_product;
                }
                
                echo json_encode($data);
                
                die;
        }
          
                
    }
}
 $order_insert = new OrderInsert;
 $order_insert->insertData();
?>        

