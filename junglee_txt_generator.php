<?php
ini_set('memory_limit', '5048M');
ini_set('max_execution_time', 30000000);
require_once 'utility.php';
require_once 'ftp.php';
define('DELIVERY_TIME', 3);
define('PAGE_SIZE',1000);
define('PRODUCTION',true);
define('NO_OF_FILES',40);
define('LIMIT',50000);

function fputcsv_eol($fp, $array, $delimeter, $encloser, $eol) {
  fputcsv($fp, $array,$delimeter,$encloser);
  if("\n" != $eol && 0 === fseek($fp, -1, SEEK_CUR)) {
    fwrite($fp, $eol);
  }
}
class JungleeAdsTxtCreate {
    
    public $mode;
    public $db;
    public $limit = 10000;
    public $domain;

    public function __construct() {
        $this->mode = PRODUCTION;
        if ($this->mode) {
            $this->domain = 'https://www.cart2india.com';

            $this->db = new mysqli('localhost', 'c2iadmin', 'C@rt2Indi@!23DB', 'pm');
        } else {
            $this->domain = 'https://www.cart2india.com';
            $this->db = new mysqli('localhost', 'root', '', 'pm_new');
        }
    }
    public function getProductCount(){
        //total query
            $cquery = ' SELECT COUNT(*) as total 
                        FROM merchant_products_master p 
                        WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND  p.india_price > 0 AND  p.priority > 0 ';
            $tresult = $this->db->query($cquery);
            $tdata = $tresult->fetch_object();
            return $tdata->total;
            
    } 
    public function generate(){

        $total_product = $this->getProductCount();
        $limit = LIMIT;

        for($i = 0; $i < NO_OF_FILES; $i++ ){
            $query = 'SELECT p.india_price, p.c2i_sku
                  FROM merchant_products_master p 
                  WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.india_price > 0 AND  p.priority > 0 ORDER BY p.priority  ';
        
            $offset = LIMIT * $i;
            $query .= ' LIMIT '.$offset. ', '.$limit;

            $result = $this->db->query($query);

            if($result->num_rows == 0){
                break;
            }
            $abso_path = getcwd().'/';           
             

            $file_name = 'junglee_price_upd_'.($i+1).'.txt';
            $source_file = $abso_path.$file_name;
            $fp = fopen($source_file, 'w');

            $header = array('Amazon.com',"Product Ads Header",'Purge-Replace=false','','');
            fputcsv_eol($fp, $header,"\t",chr(0),"\r\n");
            $header = array('SKU','Price','Availability',"Update Delete");
            fputcsv_eol($fp, $header,"\t",chr(0),"\r\n");

            while($row = $result->fetch_assoc()){
                $_data = array();
                $_data['sku'] = ltrim($row['c2i_sku'],'0');
                $_data['price'] = ceil($row['india_price']);
                $_data['availability'] = 'TRUE';
                $_data['update_delete'] = 'PartialUpdate';

                fputcsv_eol($fp, $_data,"\t",chr(0),"\r\n");
            }  

            fclose($fp);          
            ftpTransfer($source_file,$file_name);
        }
        
    }
}

$product = new JungleeAdsTxtCreate();
$product->generate();
?>

