<?php
require_once 'ftp.php';
define('DELIVERY_TIME', 3);
define('PAGE_SIZE',50000);
define('PRODUCTION',true);
//header('Content-Type: text/xml'); 

class SimpleXMLExtended extends SimpleXMLElement {
  public function addCData($cdata_text) {
    $node = dom_import_simplexml($this); 
    $no   = $node->ownerDocument; 
    $node->appendChild($no->createCDATASection($cdata_text)); 
  } 
}

function pageUrl(){
    /*if(isset($_GET['page']))
        unset($_GET['page']);
    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } 
    else 
    {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }*/
    $pageURL = 'https://www.cart2gulf.com/';
    return $pageURL;
}

function removeQuerystringVar($url) {
    $url_arr = explode('?', $url);
    return $url_arr[0];
}
$current_page = 1;
$pg_url = removeQuerystringVar(pageUrl());



$production = PRODUCTION;
if ($production) {
    $domain = 'https://www.cart2gulf.com';
    
    $mysqli = new mysqli('localhost', 'c2iadmin', 'C@rt2Indi@!23DB', 'pm');
} else {
    $domain = 'https://www.cart2gulf.com';
    
    $mysqli = new mysqli('localhost', 'root', '', 'pm_new');
}

function clean_title($content) {
    $content = html_entity_decode($content);
    $content = strip_tags($content);
    if (!mb_check_encoding($content, 'UTF-8')) {
        $content = utf8_encode($content);
    }
    $content=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $content);
    $content = trim($content);
    return $content;
}

function prepare_index_data($content) {
    $content = str_replace('&', ' ', $content);
    $content = str_replace('and', ' ', $content);
    $content = str_replace(':', ' ', $content);
    $content = str_replace('-', ' ', $content);
    $content = html_entity_decode($content);
    $content = strip_tags($content);
    if (!mb_check_encoding($content, 'UTF-8')) {
        $content = utf8_encode($content);
    }
    $content = trim($content);
    $content = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $content); // Removes special chars.
    $content = preg_replace('!\s+!', ' ', $content);
    return $content;
}

function replaceExtraString($content){
    return strip_tags(str_replace('&', ' ', $content));
}
function cleanup_text($content,$replace_optr = '-') {
        $content = html_entity_decode($content);
        $content = strip_tags($content);
        if (!mb_check_encoding($content, 'UTF-8')) {
            $content = utf8_encode($content);
        }
        $content = trim($content);
        $content = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $content); // Removes special chars.
        $content = preg_replace('!\s+!', $replace_optr , $content);
        $content=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $content);
        return strtolower($content);
    }
function WriteSiteMapData($data) {
        $fileName = "sitemap.xml";
        $log_path = getcwd(); //log folder
        $fp = fopen($log_path .DIRECTORY_SEPARATOR. $fileName, 'a+');
        fwrite($fp, $data . "\n");
        fclose($fp);
    }    
    
function getProductCount(){
    global $mysqli ;
    //total query
        $cquery = ' SELECT COUNT(*) as total 
                    FROM merchant_products_master p 
                    INNER JOIN categories c ON p.`category_id`=c.id  
                    WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0 AND  p.priority >= 6 ';
        $tresult = $mysqli->query($cquery);
        $tdata = $tresult->fetch_object();
        return $tdata->total;
        
}    

function getPages(){
    global $limit ;
    $total = getProductCount();
    
    return (int)($total/$limit) + 1;
}

function getCategoryName($category_id){
    global $mysqli ;
    $query = sprintf('SELECT c.name as category_name FROM categories c WHERE id = %d',$category_id);
    $result = $mysqli->query($query);
    
    if($result->num_rows == 0)
        return false;
    
    $row = $result->fetch_object();
    
    return $row->category_name;
}
function get_parent_nodes($category_id, &$leaves) {
        $parent_nodes = array();
        if (has_parent($category_id, $parent_nodes)) {

            $leaves[] = $parent_nodes;
            get_parent_nodes($parent_nodes['parent_id'], $leaves);
        } else {
//            if (! in_array($category_id, $leaves) && empty($leaves) && is_array($leaves))
//                $leaves[] = $category_id;
        }
    }

    //************************************************************************
    function has_parent($category_id, &$parent_nodes) {
        global $mysqli,$offset,$limit,$pg_url,$current_page,$domain ;

        $sqlQuery = "SELECT
                        id,
                        name,
                        parent_id
                    FROM 
                        categories
                    WHERE 
                        id = $category_id LIMIT 1";

        $result = $mysqli->query($sqlQuery);
        
        $data = array();
        while($row = $result->fetch_assoc()){
            $data[] = $row;
        }
        

        if (!empty($data)) {
            $parent_nodes = array_shift($data);
            return true;
        }
        return false;
    }
    
function makeBreadcumb($category_tree){
    $ch_arr = array_chunk($category_tree,3);
    $cur = current($ch_arr);
    if(!empty($cur)){
       $reversed = array_reverse($cur);
       $counter = 0;
       $breadcumb = '';
       foreach($reversed as $cat){
           $breadcumb .= $cat['name'];
           if($counter < 2)
            $breadcumb .= ' > ';           
           $counter++;
       }
       
       return utf8_encode($breadcumb);
    }
}    
function generateXml($xml,$category_head) {
    global $mysqli,$offset,$limit,$pg_url,$current_page,$domain ;
    
    
        $meta_include_arr = array('Brand' , 'Images1', 'EditorialReview1');
        
        $meta_include_arr_map = array(
            'Brand' => 'author_brand', 
            'Images1' => 'image_urls',             
            'EditorialReview1' => 'description',
            );
        
         $query = sprintf('SELECT 
                    p.uae_price, p.availability_flag, 
                    IF(p.merchant_id = 1,p.merchant_sku,"") as merchant_sku,
                    p.category_id, 
                    c.name as subsub_category, 
                    p.Title, 
                    c.category_head, 
                    c.parent_id, 
                    p.priority, 
                    p.india_price offer_price, 
                    p.uae_price, 
                    p.c2i_sku, 
                    p.shipping_cost,
                    p.category_id 
                  FROM merchant_products_master p 
                  INNER JOIN categories c ON p.`category_id`=c.id 
                  WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0 AND  p.priority >= 6 AND c.category_head = %d ORDER BY p.priority DESC  ',$category_head);
        
//         $query .= ' LIMIT 100 ';
//        $query .= ' LIMIT '.$offset. ', '.$limit;
         
        $result = $mysqli->query($query);
        
        if($result->num_rows == 0)
            return false;
       
        //generate xml string
        while ($row = $result->fetch_object()) {
            
            $meta_query = 'SELECT meta_key, meta_value FROM merchant_product_meta WHERE c2i_sku=' . $row->c2i_sku;
            $meta_result = $mysqli->query($meta_query);
            $title = cleanup_text($row->Title);
            $category_name = cleanup_text($row->subsub_category);
            $url = $domain .'/'. $category_name.'/'.$title.'/' . $row->c2i_sku ;
            $product['author_brand'] = null;
            $product['img_urls'] = null;
            $product['description'] = null;
            $product = array(
                    'name' => iconv(mb_detect_encoding($row->Title, mb_detect_order(), true), "UTF-8", $row->Title),
                    'product_id' => ltrim($row->c2i_sku,0),
                    'category_id' => ltrim($row->category_id,0),
                    'price' => intval(ceil($row->uae_price)),
                    'url' => $url,
                    'availability' => 'in stock',
                );
            
                $category_head = getCategoryName($row->category_head);
                $product['parent_category'] = $category_head;                
                $parent_category = getCategoryName($row->parent_id);
                $parent_nodes = array();
                get_parent_nodes($row->category_id,$parent_nodes);
                
                $product['breadcrumb'] = makeBreadcumb($parent_nodes);
                
                
            //get meta data
            while ($meta = $meta_result->fetch_object()){
                
                if(in_array(trim($meta->meta_key), $meta_include_arr)){
                    $product[$meta_include_arr_map[trim($meta->meta_key)]]= trim(utf8_encode(replaceExtraString($meta->meta_value)));
                }
            }
            
            //reorder the array
            $ordered_product = array();
            $ordered_product = array(
                'name' => $product['name'],
                'product_id' => $product['product_id'],
                'category_id' => $product['category_id'],
                'description' => isset($product['description'])?$product['description']:null,
                'author_brand' => $product['author_brand'],
                'price' => $product['price'],
                'breadcrumb' => $product['breadcrumb'],
                'parent_category' => $product['parent_category'],
                'url' => $product['url'],
                'availability' => $product['availability'],
                'image_urls' => $product['image_urls'],
                );
            
            // assigned to xml object 
            $item = $xml->addChild('item');
            foreach($ordered_product as $key => $value){
//                if(!in_array($key, array('sku','availability','link','india_price','delivery_time','category_id','rbn','asin','standard_product_id','offer_note')))
                
                if(in_array(strtolower(trim($key)), array('name','description'))){
                    $item->$key = NULL; // VERY IMPORTANT! We need a node where to append
                    $item->$key->addCData($value);
                }
                else {
                    $item->addChild($key, $value);
                }        
                
//                $child = $item->addChild($key);
//                $child->value = $value;
            } 
            
            unset($product);
        }
        
        return $xml ;
}
$pages = 1;

$limit = PAGE_SIZE;
$pages = getPages();

$query = sprintf('SELECT c.name, c.id as category_id FROM categories c WHERE c.category_head = 0 AND  c.id NOT IN (600000,7000000) ');
$result = $mysqli->query($query);

if($result->num_rows == 0)
    die('no category');

while ($row = $result->fetch_assoc()){
    $xml = new SimpleXMLExtended('<xml/>');
    $xml =  generateXml($xml,$row['category_id']);
    if(!$xml)
        continue;
    $abso_path = getcwd().'/';
//    $abso_path = null;
    $file_name = str_replace(' ', '_', $row['name']);
    $filename = $abso_path.'data/'.  $file_name.'.xml';
    $xml->saveXML($filename);
    $desination_folder = 'bkam';
    $destination_file = $desination_folder.'/'.$file_name.'.xml'; //where you want to throw the file on the webserver (relative to your login dir)
    ftpTransfer($filename,$destination_file,$desination_folder); 
    unlink($filename);  
    
}


?>

