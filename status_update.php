<?php 
$production = 1;
if ($production) {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'c2iadmin', 'C@rt2Indi@!23DB', 'pm');
} else {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'root', '', 'c2iportal_test');
}
class StatusUpdate{
    public $report = array();
    public function udpate(){
        global $mysqli;
        $request_data = $_POST;
        
        $credentials = $request_data['credentials'];
        $list = $request_data['list'];
        
        $cquery = sprintf('SELECT * FROM pg_clients pc WHERE  username = "%s" AND password = "%s"',$credentials['username'],$credentials['password']);
        $cresult = $mysqli->query($cquery);
        
        if(!isset($cresult->num_rows)){
            $this->report['status'] = false;
            $this->report['mesage'] = 'No record to update';
            echo json_encode($this->report);
            die;
        }
        
        if($cresult->num_rows != 1){
            $this->report['status'] = false;
            $this->report['message'] = 'Incorrect credential';
        }
        else {
            // for  valid authentication
            if(!empty($list)){
                foreach($list as $_data){
                    $query = sprintf('SELECT * FROM order_products op WHERE  op_id = %d',$_data['client_transaction_id']);
                    $cresult = $mysqli->query($query);
                    
                    $order_product = $cresult->fetch_object(); 
                    
                    if( $cresult->num_rows == 0 ){
                        $this->report[][$_data['client_transaction_id']] = $_data['client_transaction_id'].'-empty-';
                        continue;
                    }
                    $prev_status = $order_product->status_id;
                    $order_product->status_id = $_data['status_id'];
                    $_data['status_update_date'] = date('Y-m-d H:i:s');
                    
                    $up_query = sprintf('UPDATE order_products op 
                                        SET op.status_id = %d ,
                                        op.status_update_date = "%s" 
                                        WHERE  op_id = %d',
                                        $_data['status_id'],
                                        $_data['status_update_date'],
                                        $_data['client_transaction_id']);
                    
			$status = $mysqli->query($up_query);
                    if(($prev_status < $_data['status_id'])){
                        $this->report[][$_data['client_transaction_id']] = $status. '-up-';
                    } 
                    else {
                        $this->report[][$_data['client_transaction_id']] = $_data['status_id'];
                    }
                }
            }
            else {
                $this->report['status'] = false;
                $this->report['mesage'] = 'No record to update';
            }
        }
        
        
        echo json_encode($this->report);
        sleep(1);
        die;
    }
    
    public function actionInsertdata(){
        $request_data = $_POST;
        $credentials = $request_data['credentials'];
        $type = $request_data['type']; // for future use
        
        $cquery = sprintf('SELECT * FROM pg_clients pc WHERE  username = "%s" AND password = "%s"',$credentials['username'],$credentials['password']);
        $cresult = $mysqli->query($cquery);
        if($cresult->num_rows != 1){
            $this->report['status'] = false;
            $this->report['message'] = 'Incorrect credential';
        }
        else {
        $total_query  = 
                "SELECT o.order_id, 
                    op.op_id, op.description, op.quantity,  op.unit_price, op.shipping_price,o.discount_amount,o.discount_type, op.c2i_sku, 
                    a.first_name ,a.last_name, c.email, a.gender, a.company, a.address_1, a.address_2, a.state, a.city, 
                    a.post_code, co.name, a.mobile, a.landline 
                FROM  `orders` AS o
                JOIN order_products AS op ON o.order_id = op.order_id
                JOIN address AS a ON a.address_id = o.shipping_address_id
                JOIN customer AS c ON c.id = o.customer_id
                JOIN country AS co ON a.country_id = co.country_id  
                
                "; 
                
                $total_query .= " WHERE op.status_id = 1";
                $data = Yii::app()->db->createCommand($total_query)->queryAll();
                
                echo CJSON::encode($data);
                Yii::app()->end();
        }
                
                
    }
}
 $status_update = new StatusUpdate;
 $status_update->udpate();
?>        

