<?php
define('DELIVERY_TIME', 3);
define('PAGE_SIZE',5);
define('PRODUCTION',true);
header('Content-Type: text/xml'); 


function pageUrl(){
    if(isset($_GET['page']))
        unset($_GET['page']);
    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } 
    else 
    {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function removeQuerystringVar($url) {
    $url_arr = explode('?', $url);
    return $url_arr[0];
}
$current_page = isset($_GET['page'])?$_GET['page']:1;
$pg_url = removeQuerystringVar(pageUrl());


$limit = PAGE_SIZE;
$offset = ($current_page - 1) * $limit;
$production = PRODUCTION;
if ($production) {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'root', 'c@rt@indi@DB', 'pm');
} else {
    $domain = 'https://www.cart2india.com';
    
    $mysqli = new mysqli('localhost', 'root', '', 'pm_new');
}


function WriteSiteMapData($data) {
        $fileName = "sitemap.xml";
        $log_path = getcwd(); //log folder
        $fp = fopen($log_path .DIRECTORY_SEPARATOR. $fileName, 'a+');
        fwrite($fp, $data . "\n");
        fclose($fp);
    }    
function generateXml($xml) {
    global $mysqli,$offset,$limit,$pg_url,$current_page,$domain ;
    
        
        
        $query = 'SELECT p.india_price, p.c2i_sku
                  FROM merchant_products_master p 
                  WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0 AND  p.priority > 0 ';
        
        $query .= ' LIMIT '.$offset. ', '.$limit;
        $result = $mysqli->query($query);
        
        //total query
        $cquery = ' SELECT COUNT(*) as total 
                    FROM merchant_products_master p  
                    WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0 AND  p.priority > 0 ';
        $tresult = $mysqli->query($cquery);
        $tdata = $tresult->fetch_object();
        
        $pages = (int)($tdata->total/$limit) + 1; 
        
        //generate xml string
        
        $items = $xml->addChild('items');
        while ($row = $result->fetch_object()) {
            
            $product = array(
                    'sku' => ltrim($row->c2i_sku,'0'),
                    'price' => $row->india_price
                );
            
            
            // assigned to xml object 
            $item = $items->addChild('item');
            foreach($product as $key => $value){
                $item->addChild($key, $value);
            } 
            
            unset($product);
        }
        
        if($pages > $current_page){
            
            $next_page_url = $items->addChild('next_page_url');
            $next_page_url->addChild('url', $pg_url.'?page=' . ($current_page + 1));
        }
            
        
        
        return $xml ;
}

/*function generateXmlBck() {
    global $mysqli,$offset,$limit,$url,$current_page;
    $query = 'SELECT p.category_id, p.category_flag, p.created, p.category_id, c.name, p.Title, p.priority, p.india_price offer_price, p.uae_price, p.c2i_sku, pm.`meta_value` FROM merchant_products_master p INNER JOIN categories c ON p.`category_id`=c.id  INNER JOIN merchant_product_meta pm ON p.`c2i_sku`=pm.`c2i_sku` AND pm.`meta_key`="Images1" WHERE p.availability_flag = 1 AND p.scrape_flag= 1 AND p.variations_flag = 0 AND p.india_price > 0';
    $query .= ' LIMIT '.$offset. ', '.$limit;
    //total query
    $cquery = 'SELECT COUNT(*) as total FROM merchant_products_master p WHERE p.availability_flag = 1 AND p.india_price > 0 ';
    
    $tresult = $mysqli->query($cquery);
    
    $tdata = $tresult->fetch_object();
    
    $result = $mysqli->query($query);
    
    while ($row = $result->fetch_object()) {
        $brand_query = 'SELECT meta_value FROM merchant_product_meta WHERE meta_key="brand" AND c2i_sku=' . $row->c2i_sku;
        $brand_result = $mysqli->query($brand_query);
        $brand = $brand_result->fetch_object();
        if (isset($brand->meta_value))
            $brand = prepare_index_data($brand->meta_value);
        else
            $brand = '';
        $title = clean_title($row->Title);
        $search_title = prepare_index_data($row->Title);
        $url = $domain . '/product/productDetails/' . $row->c2i_sku . '/';
        $category = prepare_index_data($row->name);
        $echo .= 
              '<item>
                <sku> '.$data->c2i_sku.'</sku>
                <title>'. $data->Title.'</title>
                <link>'. $data->india_price.'</link>
                <price>'. $data->india_price.'</price>
                <price>'. $data->india_price.'</price>
                <price>'. $data->india_price.'</price>    
              </item>';
        $product_document = new \Elastica\Document($row->c2i_sku, $product);
        $elasticaType->addDocument($product_document);
        $elasticaType->getIndex()->refresh();
        unset($product);
        unset($product_document);
        unset($row);
    }
    $pages = (int)($tdata->total/$limit) + 1;
    $echo = '<xml version="1.0">';
    $echo .= '<items>';
    while ($data = $cresult->fetch_object()) {
        $current_cat = $this->cleanup_text($data->category_name);
        $echo .= 
              '<item>
                <sku> '.$data->c2i_sku.'</sku>
                <title>'. $data->Title.'</title>
                <link>'. $data->india_price.'</link>
                <price>'. $data->india_price.'</price>
                <price>'. $data->india_price.'</price>
                <price>'. $data->india_price.'</price>    
              </item>';
        
    }
    
    if($pages > $current_page)
        $echo .= '<next_page_url>'.$url.'?page=' . ($current_page + 1).'</next_page_url>';
    $echo .= '</items>';
    $echo .= '</xml>';
    
    return $echo;
}*/


$xml = new SimpleXMLElement('<xml/>');
$xml =  generateXml($xml);
print($xml->asXML());
//$xml->saveXML('sitemap.xml');
?>

